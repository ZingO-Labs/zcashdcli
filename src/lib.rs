#[macro_use]
extern crate serde;
#[derive(Debug)]
pub enum ResponseError {
    ListReceivedByAddress,
}

use serde_json::{map::Map, Value};
extern crate emflib;
use emflib::{MemoContainer, MemoContainerError, Response};
extern crate hex;
#[derive(Debug)]
pub struct AddressReceivedList {
    pub transactions: Vec<Map<String, Value>>,
}
impl Response for AddressReceivedList {}
#[derive(Debug)]
pub struct ReceivedByAddressMemos {
    pub memos_as_u8_vectors: Vec<Vec<u8>>,
}
impl ReceivedByAddressMemos {
    pub fn new(memos_source: &AddressReceivedList) -> ReceivedByAddressMemos {
        ReceivedByAddressMemos {
            memos_as_u8_vectors: <ReceivedByAddressMemos as MemoContainer>::access_memos(
                memos_source,
            )
            .unwrap(),
        }
    }
}
impl MemoContainer for ReceivedByAddressMemos {
    type Responder = AddressReceivedList;
    fn access_memos(memos_source: &Self::Responder) -> Result<Vec<Vec<u8>>, MemoContainerError> {
        let mut memos_as_bytes: Vec<Vec<u8>> = vec![];
        for t in &memos_source.transactions {
            memos_as_bytes.push(<ReceivedByAddressMemos as MemoContainer>::access_memo(&t)?);
        }
        Ok(memos_as_bytes)
    }
    fn access_memo(transaction: &Map<String, Value>) -> Result<Vec<u8>, MemoContainerError> {
        match transaction.get("memo") {
            Some(Value::String(memo)) => Ok(hex::decode(memo).unwrap()),
            _ => Err(MemoContainerError::MemoAccess),
        }
    }
}

pub trait Request {
    type Responder: Response;

    fn new(invocationargs: Vec<String>) -> Self;
    fn call(&self) -> Result<Self::Responder, ResponseError>;
}

#[derive(Debug)]
pub struct ZListReceivedByAddress {
    clirunner: String,
    command: String,
    cliargs: Vec<String>,
}

impl Request for ZListReceivedByAddress {
    type Responder = AddressReceivedList;

    fn new(mut invocationargs: Vec<String>) -> ZListReceivedByAddress {
        ZListReceivedByAddress {
            clirunner: invocationargs.remove(0),
            command: invocationargs.remove(0),
            cliargs: invocationargs,
        }
    }

    fn call(&self) -> Result<Self::Responder, ResponseError> {
        use std::process::Command;
        println!("Before output.");
        let output = Command::new(&self.command)
            .args(&self.cliargs)
            .output()
            .expect("zcash-cli call failed!");
        println!("After output.");
        use std::str;
        extern crate serde_json;
        use Value;
        let sj: Value = serde_json::from_str(&str::from_utf8(&output.stdout).unwrap()).unwrap();
        //extern crate hex;
        //use hex;
        //let asbytes: Vec<u8> = hex::decode(actual).unwrap();*/
        let mut outer: Vec<Map<String, Value>> = vec![];
        for item in sj.as_array().unwrap() {
            //("{:?}", item);
            //("{:?}", item.as_object().unwrap().to_owned());
            // XXX FIX THIS!! Here we lose our zero-copy abstraction
            outer.push(item.as_object().unwrap().to_owned());
        }
        match true {
            _n => Ok(AddressReceivedList {
                transactions: outer,
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
