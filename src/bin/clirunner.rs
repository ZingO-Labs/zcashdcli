use std::env;
extern crate emflib;
extern crate zcashdcli;
use emflib::MemoField;

use zcashdcli::{ReceivedByAddressMemos, Request, ZListReceivedByAddress};

fn main() {
    use serde::Deserialize;

    #[derive(Deserialize)]
    pub struct Info {
        username: String,
    }
    extern crate actix_web;
    use actix_web::{http, server, App, HttpRequest, Path, Query, Responder};

    fn index((path, query): (Path<(u32, String)>, Query<Info>)) -> String {
        format!("Welcome {}!", query.username)
    }

    fn index2(_req: &HttpRequest) -> impl Responder {
        "Hello world!"
    }
    server::new(|| {
        vec![
            App::new().resource(
                "/users/{username}/{friend}", // <- define path parameters
                |r| r.method(http::Method::GET).with(index),
            ),
            App::new().resource("/", |r| r.method(http::Method::GET).f(index2)),
        ]
    })
    .bind("0.0.0.0:8080")
    .unwrap()
    .run();
    let arguments: Vec<String> = env::args().collect();
    //println!("arguments: {:?}", arguments);
    let zcashdcli = ZListReceivedByAddress::new(arguments);
    //println!("zcashdcli: {:?}", zcashdcli);
    let receivedtransactions = zcashdcli.call().unwrap();
    //println!(
    //    "AddressReceivedList.transactions: {:?}",
    //    receivedtransactions.transactions
    //);
    let receivedbyaddressmemos = ReceivedByAddressMemos::new(&receivedtransactions);
    let memofield0 = MemoField::new(&receivedbyaddressmemos.memos_as_u8_vectors[0]).unwrap();
    //println!("{:?}", memofield0);
    //println!("{:?}", receivedbyaddressmemos.memos_as_u8_vectors.len());
    //println!("{:?}", receivedbyaddressmemos.memos_as_u8_vectors);
    use std::borrow::Cow;
    let result = match MemoField::new(&receivedbyaddressmemos.memos_as_u8_vectors[1]) {
        Ok(MemoField::UTF8(res)) => res,
        Ok(MemoField::Registration(res)) => Cow::Borrowed("spam"),
        Err(Error) => Cow::Borrowed("foo"),
    };
    //println!("{:?}", result);
}
